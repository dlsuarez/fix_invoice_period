# -*- coding: utf-8 -*-
###############################################################################
#
#    Odoo, Open Source Management Solution
#
#    Copyright (c) All rights reserved:
#        (c) 2015  
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses
#
###############################################################################
{
    'name': 'Fix invoice period',
    'summary': 'Fix invoice period Module',
    'version': '1.0',
    'description': """
Fix invoice period Module.
==============================================

    """,
    'author': 'Anubia, Soluciones en la Nube, SL',
    'website': "http://www.anubia.es",
    'maintainer': 'Anubia, soluciones en la nube, SL',
    'contributors': [
        'Daniel Lago Suárez <dlagosuarez@gmail.com>'
    ],
    'website': 'https://gitlab.com/dlsuarez/fix_invoice_period/',
    'license': 'AGPL-3',
    'category': 'Account',
    'depends': [
        'account'
    ],
    'external_dependencies': {
        'python': []
    },
    'data': [],
    'demo': [],
    'js': [],
    'css': [],
    'qweb': [],
    'images': [],
    'test': [],
    'installable': True
}
