# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __openerp__.py file at the root folder of this module.                   #
###############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import Warning
from datetime import datetime, date, time, timedelta
import calendar
import logging

class account_invoice(models.Model):

    _inherit = 'account.invoice'

    _logger = logging.getLogger(__name__)

    @api.model
    def _change_period(self):
        for period in self.env['account.period'].\
           search([('special', '=', False)]):
            changed_date_invoice = fields.Datetime.from_string(self.date_invoice)
            date_start_period = fields.Datetime.from_string(period.date_start)
            date_end_period = fields.Datetime.from_string(period.date_stop)
            if changed_date_invoice >= date_start_period and \
                changed_date_invoice <= date_end_period:
                self.period_id = period.id

    @api.multi
    def write(self, values):
        result = super(account_invoice, self.sudo()).write(values)
        if 'date_invoice' in values and self.date_invoice:
            self._change_period()
        return result
